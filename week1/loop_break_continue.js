function pickIt (arr) {

    const odd = []
    const even =[]
        
    for (let x of arr) {
        ((x % 2) ? odd : even).push(x)    
    }
        
    return [odd, even]
}