function converter(roman) {
    const romNum= ['I', 'V', 'X', 'L', 'C', 'D', 'M']
    const intNum= [1, 5, 10, 50, 100, 500, 1000]

    let result=0;
    for(i=0;i<roman.length;i++) {
        const currentRom= romNum.indexOf(roman[i]);
        const nextRom= romNum.indexOf(roman[i+1]);

        const currentNum= intNum(currentRom);
        const nextNum= intNum(nextRom);

        if(currentNum<nextNum) {
            result-=currentNum;
        } else {
            result+=currentNum;
        }
    }
    return result
}

console.log(converter('XVII'));